FROM python:3.8.0-alpine3.10
ENV PYTHON_UNBUFFERED 1
WORKDIR /data
COPY requirements.txt ./
RUN apk add --no-cache tini==0.18.0-r0  && \
		apk add --no-cache --virtual build-dependencies \
			libffi-dev==3.2.1-r6 \
			libressl-dev==2.7.5-r0 \
			build-base==0.5-r1 \
			gcc==8.3.0-r0 && \
    pip install --no-cache-dir -r requirements.txt && \
    pip uninstall pip -y && \
    rm requirements.txt && \
		apk del build-dependencies && \
    adduser -D -u 1000 ansiblelint

USER ansiblelint
ENTRYPOINT ["/sbin/tini", "--", "ansible-lint" ]
CMD ["--help"]

LABEL org.label-schema.name="ansiblelint" \
      org.label-schema.description="--" \
      org.label-schema.vendor="--" \
      org.label-schema.docker.schema-version="1.0" \
      org.label-schema.vcs-type="git"
