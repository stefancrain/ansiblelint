# Docker ansiblelint

[![pipeline status](https://gitlab.com/stefancrain/ansiblelint/badges/master/pipeline.svg)](https://gitlab.com/stefancrain/ansiblelint/commits/master)
[![coverage report](https://gitlab.com/stefancrain/ansiblelint/badges/master/coverage.svg)](https://gitlab.com/stefancrain/ansiblelint/commits/master)

![image](https://dockeri.co/image/stefancrain/ansiblelint?&kill_cache=1)

A docker ansiblelint built with :

- [Docker Content Trust (DCT)](https://docs.docker.com/engine/security/trust/content_trust/) for image signing and verification
- [Docker Buildx](https://docs.docker.com/buildx/working-with-buildx/) for multiple platform images builder
- Image security review with [clair](https://github.com/arminc/clair-scanner)
- Static code analysis from [gitlabs tools](https://docs.gitlab.com/ee/user/application_security/sast/)

## Running

```bash
docker pull stefancrain/ansiblelint:latest
docker run -it --rm stefancrain/ansiblelint:latest
docker inspect stefancrain/ansiblelint:latest | jq
docker manifest inspect stefancrain/ansiblelint:latest | jq
docker buildx imagetools inspect stefancrain/ansiblelint:latest
```

## Supported architectures

- amd64
- arm32v6
- arm32v7
- arm64v8
- i386
- ppc64le
- s390x

## container info

- Clair Help : [1](https://forge.etsi.org/rep/help/ci/examples/container_scanning.md)[2](https://www.tigera.io/blog/adding-cve-scanning-to-a-ci-cd-pipeline/)
- [PYTHON_UNBUFFERED](https://github.com/awslabs/amazon-sagemaker-examples/issues/319#issuecomment-405749895)
- [label-schema](http://label-schema.org/rc1/)
- [buildx](https://gist.github.com/merlinschumacher/024513b3d5e4b9cfd13f41f4a2b962b1)
- [buildx](https://circleci.com/orbs/registry/orb/arribada/docker)

## License

The package is distributed under the [MIT](LICENSE) license.
